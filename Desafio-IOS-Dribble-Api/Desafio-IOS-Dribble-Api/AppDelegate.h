//
//  AppDelegate.h
//  Desafio-IOS-Dribble-Api
//
//  Created by Dinesh Trivedi on 9/21/15.
//  Copyright (c) 2015 Dinesh Trivedi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

